# packagegroup definitions to help the SDR community build images
# they like.

LICENSE = "MIT"

inherit packagegroup

SDR_PACKAGES = "\
    python-mako \
    python3-cython \
    python3-numpy \
    python3-modules \
    python3-six \
    python3-twisted \
    gnuradio \
    gnuradio-analog gnuradio-blocks \
    gnuradio-channels gnuradio-digital gnuradio-fec gnuradio-fft \
    gnuradio-filter gnuradio-gr gnuradio-gru \
    gnuradio-gr-utils \
    gnuradio-runtime \
    gnuradio-trellis \
    gnuradio-wavelet \
    gnuradio-zeromq \
    gnuradio-grc \
    libiio-iiod \
    libiio-tests \
    libiio-python \ 
"

RDEPENDS_${PN}_append += " \
${SDR_PACKAGES} \
"
